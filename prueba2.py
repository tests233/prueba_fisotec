from fisotec_basedatos import *

def createTableAgrupacion(connection):
    """
        Crea una tabla Agrupacion a partir de la tabla luminarias
        :param:      conexion
    """
    query = """CREATE TABLE IF NOT EXISTS alcaudete.agrupacion AS select COUNT(*) as num_luminarias, lm.tsoporte, lm.tluminaria, lm.tlampara, lm.altura, lm.potencia, lm.tipovia, vl.nombre
            from alcaudete.luminaria lm
            inner join alcaudete.vial vl on vl.id = lm.vial
            GROUP BY lm.tsoporte, lm.tluminaria, lm.tlampara, lm.altura, lm.potencia, lm.tipovia, vl.nombre"""
    data = FisotecBaseDatos.consultaSQL(connection, query)
    print("completado, tabla agrupacion creada!.")

connection = FisotecBaseDatos.conectarBaseDatos()
createTableAgrupacion(connection)