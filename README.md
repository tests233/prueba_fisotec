# Python y Postgis
## Instrucciones DB
* Ejecutamos la construcción de los contenedore
```
docker-compose build
```
* ejecutamos el contenedor de la base de datos
```
docker-compose up database
```
Ingresamos al panel del postgres

```
docker-compose exec database psql -U postgres template1
```
* Creamos la base de datos
```
template1=# create database prueba TEMPLATE template_postgis;
```

* cargamos el backup, el 01582a9fc709 el el identificador con lo que se creo mi contenedor
```
cat base_datos.backup | docker exec -i 01582a9fc709 pg_restore -U postgres -d prueba
```

## En caso de no tener un ambiente python con psycopg2-binary
* Se puede crear un contenedor
* Para esto ejecutamos el siguiente comando y ingresamos al contenedor para tener un ambiente con psycopg2-binary
```
docker-compose run --rm --service-ports app sh
```


## Ejecutar las pruebas
* Para la prueba 1 solo ejeuctar el siguiente comando
```
python prueba1.py 
```
* Para la prueba 2 solo ejecutar el siguiente comando
```
python prueba2.py 
```
* Para la prueba 3 solo ejecutar el siguiente comando
```
python prueba3.py 
```

## Modificaciones Warning solo cuando utilizas docker

Se modifico el archivo credenciales DBHOST = 'database' 

## El Backup generado se realizo con el gestor PGAdmin
se llama db_pruebas.backup
