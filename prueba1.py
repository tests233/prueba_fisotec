from fisotec_basedatos import *

def agregarCampo(conexion):
    """
        Agrega un campos a la tabla vial .
        :param:      conexion
    """
    try:
        query = """ALTER TABLE alcaudete.luminaria ADD COLUMN vial bigint;"""
        FisotecBaseDatos.consultaSQL(conexion,query)
        print("Campo agregado exitosamente!")
    except Exception as error:
        print(error)

def actualizarLuminarias(conexion):
    """
        Actualiza los campos distancia y val de la tabla  luminaria.
        :param:      conexion
    """
    querySelectLuminarias = """SELECT * FROM alcaudete.luminaria ORDER BY id ASC"""
    listaLuminarias = FisotecBaseDatos.consultaSQL(conexion, querySelectLuminarias)
    
    print("Espere se esta actualizando las Luminarias....")
    for luminaria in listaLuminarias:
        queryVial = f"""SELECT t1.id, ST_Distance(t1.geom, '{luminaria['geom']}') as distancia FROM alcaudete.vial as t1 ORDER BY distancia ASC LIMIT 1;"""
        viales = FisotecBaseDatos.consultaSQL(conexion, queryVial)
        if len(viales)>0:
            vial=viales[0]
            queryUpdateLuminaria= f"""UPDATE alcaudete.luminaria SET distancia_eje={vial['distancia']}, vial={vial['id']} WHERE id={luminaria['id']}"""
            FisotecBaseDatos.consultaSQL(conexion, queryUpdateLuminaria)
    print("Luminarias Actualizada!")

conexion= FisotecBaseDatos.conectarBaseDatos()
agregarCampo(conexion)
actualizarLuminarias(conexion)
