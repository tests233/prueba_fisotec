from fisotec_basedatos import *

def crearTabla(conexion):
    """
        Crea la tabla proyeccion a partir de la tabla luminaria.
        :param:      conexion
    """
    try:
        query = """CREATE TABLE IF NOT EXISTS alcaudete.proyeccion AS SELECT * FROM alcaudete.luminaria"""
        FisotecBaseDatos.consultaSQL(conexion,query)
        print('Tabla proyeccion creada exitosmente!')
    except Exception as error:
        print(error)

def actualizarProyeccion(conexion):
    """
        Actualiza el campo geom de luminara a partir del vial
        :param:      conexion
    """
    query = """SELECT lm.id, ST_ClosestPoint(vl.geom, lm.geom) nueva_geom
            FROM alcaudete.proyeccion  lm 
            inner join alcaudete.vial vl on  lm.vial = vl.id
            order by lm.id;"""
    projections = FisotecBaseDatos.consultaSQL(conexion, query)
    print("Espere actualizando proyeccion...")
    for projection in projections:
        update_query = f"UPDATE alcaudete.proyeccion SET geom = '{projection['nueva_geom']}' WHERE id={projection['id']}"
        FisotecBaseDatos.consultaSQL( conexion, update_query)

    print("Actulizacion completada")

conexion= FisotecBaseDatos.conectarBaseDatos()
crearTabla(conexion)

actualizarProyeccion(conexion)
