#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Global constant data.
"""

#DATABASE
DBHOST = 'database'

DBNAME = 'prueba'

DBPORT = '5432'

# Usuario administrador base de datos
DBUSER = 'postgres'
DBPASSWORD = 'postgres'